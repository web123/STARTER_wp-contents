<?php

if(!function_exists('qode_news_add_video_layout2_shortcodes')) {
	function qode_news_add_video_layout2_shortcodes($shortcodes_class_name) {
		$shortcodes = array(
			'qodeNews\CPT\Shortcodes\VideoLayout2\VideoLayout2'
		);
		
		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);
		
		return $shortcodes_class_name;
	}
	
	add_filter('qode_news_filter_add_vc_shortcode', 'qode_news_add_video_layout2_shortcodes');
}

if ( ! function_exists( 'qode_news_set_video_layout2_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for video layout 2 shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function qode_news_set_video_layout2_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-video-layout2';

		return $shortcodes_icon_class_array;
	}

	add_filter( 'qode_news_filter_add_vc_shortcodes_custom_icon_class', 'qode_news_set_video_layout2_icon_class_name_for_vc_shortcodes' );
}