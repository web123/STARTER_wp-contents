<?php

if(!function_exists('qode_news_add_layout1_shortcodes')) {
	function qode_news_add_layout1_shortcodes($shortcodes_class_name) {
		$shortcodes = array(
			'qodeNews\CPT\Shortcodes\Layout1\Layout1'
		);
		
		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);
		
		return $shortcodes_class_name;
	}
	
	add_filter('qode_news_filter_add_vc_shortcode', 'qode_news_add_layout1_shortcodes');
}

if ( ! function_exists( 'qode_news_set_layout1_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for layout 1 shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function qode_news_set_layout1_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-layout1';

		return $shortcodes_icon_class_array;
	}

	add_filter( 'qode_news_filter_add_vc_shortcodes_custom_icon_class', 'qode_news_set_layout1_icon_class_name_for_vc_shortcodes' );
}